<?php
require('animal.php');
require('frog.php');
require('ape.php');

$sheep = new Animal("sheep");

echo "Name: " . $sheep->name . "<br>";
echo "Legs: " . $sheep->legs . "<br>";
echo "Cool Blooded: " . $sheep->cold_blooded . "<br><br>";

$kodok = new Frog("buduk");
echo "Name: " . $kodok->name . "<br>";
echo "Legs: " . $kodok->legs . "<br>";
echo "Cool Blooded: " . $kodok->cold_blooded . "<br>";
echo "Jump: " . $kodok->jump . "<br><br>";

$sungokong = new Ape("kera sakti");
echo "Name: " . $sungokong->name . "<br>";
echo "Legs: " . $sungokong->legs . "<br>";
echo "Cool Blooded: " . $sungokong->cold_blooded . "<br>";
echo "Jump: " . $sungokong->yell . "<br><br>";
